import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\VASILY~1.ROM\\AppData\\Local\\Temp\\Katalon\\Test Cases\\3_iBoats Doc\\Verify separate billing and shipping address options\\20180731_114415\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCaseRawScript(
'''import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/1_Open Web Site'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(5)

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/2_Close Baner'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(5)

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/3_Open PLP'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(5)

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/4_Open PDP - First elem'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/5_Checkout Preconditions'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(3)

WebUI.click(findTestObject('3_Checkout/5_Billing Not the Same as shipping/1_Check box'))

WebUI.setText(findTestObject('3_Checkout/5_Billing Not the Same as shipping/2_First Name'), 'Tom')

WebUI.setText(findTestObject('3_Checkout/5_Billing Not the Same as shipping/3_Last Name'), 'Riddle')

WebUI.setText(findTestObject('3_Checkout/5_Billing Not the Same as shipping/4_Street Address'), '567 Test Dr.')

WebUI.setText(findTestObject('3_Checkout/5_Billing Not the Same as shipping/5_City'), 'Austin')

WebUI.selectOptionByLabel(findTestObject('3_Checkout/5_Billing Not the Same as shipping/6_State'), 'Texas', false)

WebUI.setText(findTestObject('3_Checkout/5_Billing Not the Same as shipping/8_ZIP'), '77055')

WebUI.setText(findTestObject('3_Checkout/5_Billing Not the Same as shipping/9_Phone Number'), '9874563215')

WebUI.click(findTestObject('3_Checkout/5_Billing Not the Same as shipping/7_update button'))

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/6_Checkout_CC'), [:], FailureHandling.STOP_ON_FAILURE)

''', 'Test Cases/3_iBoats Doc/Verify separate billing and shipping address options', new TestCaseBinding('Test Cases/3_iBoats Doc/Verify separate billing and shipping address options',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
