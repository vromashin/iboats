import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\VASILY~1.ROM\\AppData\\Local\\Temp\\Katalon\\Test Cases\\3_iBoats Doc\\WIP\\7_Verify product counts in top right are correct\\20180731_121835\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCaseRawScript(
'''import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.util.regex.Pattern as Pattern
import java.util.regex.Matcher as Matcher

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/1_Open Web Site'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(5)

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/2_Close Baner'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(5)

not_run: WebUI.callTestCase(findTestCase('1_GIVEN/3_Open PLP'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(5)

def brandItems = WebUI.getText(findTestObject('5_Category/5_Get Brand Number of Items'))

//WebUI.click(findTestObject('5_Category/1_Apply Brand Filter'))
WebUI.delay(5)

def prodAmount = WebUI.getText(findTestObject('misc/2_Product Amount'))

//WebUI.verifyMatch(brandItems, prodAmount, false)
Pattern regexPat = Pattern.compile('/d')

Matcher mat = regexPat.matcher(brandItems)

if (mat.find()) {
    String result = mat.group()

    println(result)
}

''', 'Test Cases/3_iBoats Doc/WIP/7_Verify product counts in top right are correct', new TestCaseBinding('Test Cases/3_iBoats Doc/WIP/7_Verify product counts in top right are correct',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
