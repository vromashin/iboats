package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object uat2_URL
     
    /**
     * <p></p>
     */
    public static Object coupon
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['uat2_URL' : 'https://iboats:iboats123!@qa1.iboats.com/shop', 'coupon' : 'mWN5Vy9NJ8pEaR4R'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        uat2_URL = selectedVariables['uat2_URL']
        coupon = selectedVariables['coupon']
        
    }
}
