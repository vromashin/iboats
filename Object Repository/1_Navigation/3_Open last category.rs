<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>3_Open last category</name>
   <tag></tag>
   <elementGuidId>ae71cf2b-efc8-4d6a-9df0-41f749466825</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ol[@class='items']/li[last()]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
