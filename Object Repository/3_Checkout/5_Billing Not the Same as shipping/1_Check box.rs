<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1_Check box</name>
   <tag></tag>
   <elementGuidId>681b9806-2649-4469-8c72-a95a6d6ccb11</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[contains(@name, 'billing-address')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
