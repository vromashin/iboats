<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2_Get Brand Text</name>
   <tag></tag>
   <elementGuidId>9f1b9bfb-0a2d-4560-84dd-bf68d8e1d7f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[contains(., 'brand')]/li[1]//span[@class='label']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
