<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>4_sorting</name>
   <tag></tag>
   <elementGuidId>1f2d254e-c0eb-4d9b-b2a6-658acb7e6889</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class, 'toolbar')][1]//div[contains(@class, 'sorter')]/select/option[last()]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
