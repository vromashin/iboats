<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>5_Get Brand Number of Items</name>
   <tag></tag>
   <elementGuidId>0541d539-2d24-4abd-a533-39ae99a76568</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[contains(., 'brand')]/li[6]//span[@class='count']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
