<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>6_verify discount error message</name>
   <tag></tag>
   <elementGuidId>2744f78a-905c-4734-a2a7-f2d68c91a876</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-role='checkout-messages']//div[@data-ui-id='checkout-cart-validationmessages-message-error']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
