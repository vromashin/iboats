import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.util.regex.Pattern
import java.util.regex.Matcher

WebUI.callTestCase(findTestCase('1_GIVEN/1_Open Web Site'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('1_GIVEN/2_Close Baner'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('1_GIVEN/3_Open PLP'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

def brandItems = WebUI.getText(findTestObject('5_Category/5_Get Brand Number of Items'))

WebUI.click(findTestObject('5_Category/1_Apply Brand Filter'))

WebUI.delay(5)

def prodAmount = WebUI.getText(findTestObject('misc/2_Product Amount'))

//regex to get item counts
Pattern regexPat = Pattern.compile('\\d+')
Matcher mat = regexPat.matcher(brandItems)
Matcher mat2 = regexPat.matcher(prodAmount)
String resultBrand = null
String resultСount = null
if(mat.find()) {
	resultBrand = mat.group()
	System.out.println(resultBrand)
}
if(mat2.find()) {
	resultСount = mat2.group()
	System.out.println(resultСount)
}

WebUI.verifyMatch(resultBrand, resultСount, false)

