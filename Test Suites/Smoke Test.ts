<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Smoke Test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-23T13:25:31</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>c585d238-c5a8-4ed2-a84e-84527cd6b36d</testSuiteGuid>
   <testCaseLink>
      <guid>d085e00c-d208-442e-abf3-90f5ff9d63dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/1_Access main page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>608e5161-9e09-445c-8491-9061ca3d10f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/2_Open PLP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7a59d32-6acf-4dec-b0a1-a9d19289a892</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/3_Open PDP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0b5e339-fcbd-4f91-9041-f765859d5725</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/4_Verify Tabs on PDP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d084fc20-2c2b-4024-9ec4-74c0901e6aed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/5_Product Added to the Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25a12fee-c4fb-4c6b-a045-43ed2b653a33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/6_Checkout Using CC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b023e8f-8d94-46ee-9b2e-55021536d27b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/7_Checkout with PO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47cc06ce-c59b-45f2-9466-c5ec1fc14147</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/8_User can Continue Shopping</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cc1499e-7a57-4dac-bb9f-b5253cd646ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/9_Quantity change of the product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95679fc6-64c6-42b4-af32-b594e110cbca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/10_delete item from cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>412c6f57-8c9c-40b0-be62-116e14ab8b8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/11_product can be updated using edit cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7249ceb1-1829-4578-8729-a90bf6e5aa8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/12_User can open category using sidebar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3f349d1-6c30-4203-94bd-2e450061096c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/13_Apply Brand Filter on CLP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54e8df35-8012-41ba-b229-1b035d802e2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/14_User can clear filters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6458c4ae-43e2-4e0e-b1be-de789ce2c407</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/15_User can sort products</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdaa8965-1147-4de0-a018-65d56ca6222f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/16_Check that user can not use expired coupon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b4cddff-b2ed-414b-a1b6-f9fda5442d78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/17_Check that user can apply coupon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a7b4903-e06e-443b-b05b-f987c14bbe0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/18_Cancel coupon code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ca36913-720e-4a59-86ea-2ece869b9214</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/19_Change shipping method</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>738c5374-f08b-4e36-bf1c-c524e748e8f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_SMOKE TEST/20_Verify Prop finder works</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
